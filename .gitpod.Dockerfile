FROM gitpod/workspace-elixir

USER gitpod

ENV KERL_INSTALL_HTMLDOCS=no
ENV KERL_INSTALL_MANPAGES=no

RUN asdf install erlang 26.1.2
RUN asdf install elixir 1.15.7-otp-26
RUN asdf global erlang 26.1.2
RUN asdf global elixir 1.15.7-otp-26
